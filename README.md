# Datos elecciones presidenciales argentina 2003 a 2015

Mi intento de organizar el caos en https://www.argentina.gob.ar/interior/dine/resultadosyestadisticas

Bajé y ordené las presidenciales, de 2003 a 2015.

Seguramente partes del código sirvan para todo lo demás.

## Contenidos
elecciones.R hace toda la magia usando lo que hay en data/

En la carpeta R hay otro script (jugar.R) que arma un gráfico de ejemplo. Me interesaba la evolución de los votos nulos, en blanco y los que consideré de izquierda.

Seguramente el PO/PTS y compañía no estén de acuerdo con mi clasificación jeje.

## Licencias
Leer LICENCIA.txt y LICENCE.md

## Datos de otras elecciones

Si buscan en github hay varios repos para complementar esto: https://github.com/search?q=elecciones+argentina

En particular, hay uno de las paso 2019 para quien se interese.